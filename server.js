const { response } = require('express')
const express = require ('express')
const { request } = require('https')
const database = require('mime-db')
const mysql =require('mysql2')
const cors =require('cors')

const app=express()
app.use(cors('*'))
app.use(express.json())

const openDatabaseConnection =()=>{

    const connection = mysql.createConnection({
        port:3306,
      
        host:'d3_56722_atul_db_1',
        user:'root',
        password: 'root',
        database : 'db'
    })
    connection.connect()
    return connection

} 


app.post('/add', (request, response)=>{
    const connection = openDatabaseConnection ()
    const { movie_id, movie_title, movie_release_date,movie_time,director_name}= request.body
    const statement=`
        insert into Movie
          (movie_id,  movie_title, movie_release_date,movie_time,director_name)
        values
          ( '${movie_title}','${movie_release_date}', '${movie_time}','${director_name}')
      `
    connection.query(statement, (error,result)=>{
        connection.end()
        if (error){
            response.send(error)
        } else{
            response.send(result)
        }
    })
    
})
app.get('/get', (request, response)=>{
    const connection = openDatabaseConnection ()
    const { movie_name}= request.body
    const statement=`
        select * from Movie where movie_name = '${movie_name}'
      `
    connection.query(statement, (error,result)=>{
        connection.end()
        if (error){
            response.send(error)
        } else{
            response.send(result)
        }
    })
    
})
app.put('/update', (request, response)=>{
    const connection = openDatabaseConnection ()
    const { movie_id, movie_release_date,movie_time }= request.body
    const statement=`
        Update Movie set movie_release_date='${movie_release_date}',movie_time='${movie_time} where movie_id='${movie_id}'`
      
    connection.query(statement, (error,result)=>{
        connection.end()
        if (error){
            response.send(error)
        } else{
            response.send(result)
        }
    })
    
})
app.delete('/delete', (request, response)=>{
    const connection = openDatabaseConnection ()
    const { movie_id}= request.body
    const statement=`
       delete from Movie  where movie_id=${movie_id}
      `
    connection.query(statement, (error,result)=>{
        connection.end()
        if (error){
            response.send(error)
        } else{
            response.send(result)
        }
    })
    
})








app.listen(4000, '0.0.0.0', ()=>{
    console.log(`server started on port 4000`)
})